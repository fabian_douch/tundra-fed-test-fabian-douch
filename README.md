# Tundra Frontend Test

## Hello and welcome

If you are here to take the Tundra Frontend Developer Test, you are in the right place. This is a public repository with read-only access.

To get set up, you will need to

1. [Create a free Bitbucket account](https://id.atlassian.com/signup)
2. Fork this repository under your new account

    ![Bitbucket Fork](fork.png "Bitbucket Fork")

3. Name the repository `tundra-fed-test-<your-name>` and leave it public

## Tools

This test is unopinionated in regards to the build tools, frameworks and/or libraries you wish to use. 

We have provided a boilerplate src folder structure to get you started, but feel free to replace this with your own.

## Your task

-   To complete the test you must implement a single web page based on the design at the following Figma link: https://www.figma.com/file/VXykAZ1vNXu3no3NYa3qaW/Tundra-FED-Test. If you do not already have one, you will need to create a free Figma account, which you will be prompted to do when loading the link.
-   You must implement both breakpoints depicted in the designs in a responsive manner.
-   The design has an interactive table element, which can be built in any JavaScript library you feel comfortable with but must function in the following way:

    -   If 1 or less optional services are selected, the Get a quote button is disabled and an error is shown asking users to select 2 or more services (not depicted in the designs, and up to you how this should look)
    -   If 2 or more optional services are selected, the Get a quote button is enabled
    -   If 6 or more optional services are selected, the Get a quote button is disabled and an error is shown asking users to select 5 or fewer services
    -   If the user refreshes the page, their selection should remain

## Notes

Browser Requirements

-   Chrome (Latest)
-   Firefox (Latest)
-   Edge (Latest)
-   Chrome Mobile (Latest)
-   Mobile Safari (Latest)

### Links

Any buttons and links can just point to #.

### Fonts

There are two fonts used in this design. Open Sans for body copy and Varta for headings. Both of these are available via Google Fonts.
It is up to you how you include the fonts on the page.

### Questions

If you have any trouble getting set up, please dont hesitate to get in touch with Aidan at [aidan@tundra.com.au](aidan@tundra.com.au)

### Submission

Email the link to your repository to [aidan@tundra.com.au](aidan@tundra.com.au), and include "Tundra FED Test - your name" as the subject line.
